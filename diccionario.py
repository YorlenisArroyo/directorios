if __name__ == "__main__":

#crear diccionario
    equipos_lpf ={

        'Arabe Unido': 15,
        'CAI': 2,
        'Tauro': 13,
        'San Francisco': 10,
        'Plaza Amador': 7,
        'Universitario': 2,
        'Sporting SM': 1

    }
print(equipos_lpf )
#imprimir informacion del elemento en especifico
print(equipos_lpf['CAI'])

#agregar un elemento
equipos_lpf['Alianza FC'] = 1
print(equipos_lpf)
#sustitucion
equipos_lpf['Alianza FC'] = 0
print(equipos_lpf)

#eliminar una llave (del)
del equipos_lpf['Alianza FC']
print(equipos_lpf)

#traer el valor de un registro
print(equipos_lpf.get('Arabe Unido'))
print(equipos_lpf.get('Atletico Veraguense'))

#imprimir las distintas duplas del diccionario en una lista, convierte en duplas listas
e = equipos_lpf.items()
print(e)

#
e1 = equipos_lpf.keys()
print(e1)
#imrime lista de valores
e2= equipos_lpf.values()
print(e2)
# elimina valores del diccionario igual que el del
e3= equipos_lpf.pop('CAI')
print(equipos_lpf)
#clear= elimina los registros del diccionario
equipos_lpf.clear()
print(equipos_lpf)